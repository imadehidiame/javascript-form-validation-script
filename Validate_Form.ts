export class Validate_Form{

    private validate_object_test;
    constructor(form_name:string){
        this.validate_object_test = {
            'required':function(value){
                return value.trim()==''?false:true;
            },
            'min_length_check':function(value,count){
                return value.trim().length < count?false:true;
            },
            'valid_email':function(value){
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value.toLowerCase());    
            },
            'reg_exp':function(value,reg){
                var check = false;
                for(var i = 0; i< reg.length; i++){
                   
                    var testing:any = new String(reg[i])
                    var re = new RegExp(testing);
                   
                    check = check || re.test(value);
                    if(check)
                    break;
                }
                return check;
            },
            'reg_exp_dependent':function(value:any,target_element:any){
                var country_reg_exp  = {
                    "Nigeria": { 
                       reg_exp: [/(^0[7-9][0-1])[0-9]{8}$/,/(^234[7-9][0-1])[0-9]{8}$/,/(^2340[7-9][0-1])[0-9]{8}$/,/(^\+234[7-9][0-1])[0-9]{8}$/,/(^\+2340[7-9][0-1])[0-9]{8}$/],
                       id_length:10  
                    },
                    "Ghana": {
                       reg_exp: [/(^2330)[0-9]{9}$/,/(^\+233)[0-9]{9}$/,/(^233)[0-9]{9}$/],
                       id_length:12
                    }, 
                    "India": {
                        reg_exp: [/(^\+91)[0-9]{15}$/],
                        id_length:15
                     },
                     "United States of America":{
                         reg_exp: [/(^\+1)[0-9]{10}$/],
                         id_length:11
                     },
                     'Others':{
                        reg_exp: [/(^\+[1-9])[0-9]{*}$/],
                     }
                   };
                var check = false;
                if(country_reg_exp.hasOwnProperty(document.querySelector('[name="'+target_element+'"]')['value'])){
                    var reg = country_reg_exp[document.querySelector('[name="'+target_element+'"]')['value']]['reg_exp'];
                    for(var i = 0; i< reg.length; i++){
                       
                        var testing:any = new String(reg[i])
                        var re = new RegExp(testing);
                       
                        check = check || re.test(value);
                        if(check)
                        break;
                    }
                    return check;
                }
        
                var reg:any = country_reg_exp['Others']['reg_exp'];
                    for(var i = 0; i< reg.length; i++){
                       
                        var testing:any = new String(reg[i])
                        var re = new RegExp(testing);
                       
                        check = check || re.test(value);
                        if(check)
                        break;
                    }
                    return check;
        
                
            },
            'valid_number':function(value){
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
            },
            'equals':function(value,target_element){
                return value === document.querySelector('[name="'+target_element+'"]')['value'];
            },
            'notequals':function(value,target_element){
                return value !== document.querySelector('[name="'+target_element+'"]')['value'];
            }
        };
        var ultimate_form_object = this.create_form_object(form_name);
        var class_object = this;
        this.validate(ultimate_form_object,form_name);        
        this.add_ev_listener(document.forms[form_name],
        function(){
            return class_object.validate_object(ultimate_form_object);
        },'onsubmit');

    }
    private create_form_object(form_name:string):any{
        var obb = {};
            for(var i=0;i<document.forms[form_name].length;i++){
                if(document.forms[form_name].elements[i].getAttribute('type')=='submit'||document.forms[form_name].elements[i].getAttribute('type')=='hidden'||document.forms[form_name].elements[i].getAttribute('type')=='button'||!document.forms[form_name].elements[i].dataset.rules)
                continue;
                //var count = i;
                obb[document.forms[form_name].elements[i].getAttribute('name')]={
                    'count':document.forms[form_name].elements[i].parentElement.childElementCount,
                    'rules':document.forms[form_name].elements[i].dataset.rules.split('|'),
                    'messages':document.forms[form_name].elements[i].dataset.messages.split('|'),
                    'valid':false,
                    'element':document.forms[form_name].elements[i],
                    'element_name':document.forms[form_name].elements[i].getAttribute('name'),
                    'valid_messages':function(){
                        var message_object = {};
                        for(var u = 0; u< this.messages; u++){
                            message_object[this.messages[u].split('-')[0]]=this.messages[u].split('-')[1];
                            
                        }
                        return message_object;
                    }
                    
                };
                
            }
            return obb;
        
        }
        private add_ev_listener(element:any,funct:any,ev_type:string):void{
            element[ev_type]=funct;
         }
         private create_error_messages(obj:any,key:any):any{
            var ob = {};
            var messages = obj[key]['messages'];
            for(var i = 0; i< messages.length; i++){
                ob[messages[i].split('-')[0]]=messages[i].split('-')[1];
            }
            return ob;
        }
        
        private validate_object(obj:any):any{
            var valid = true;
            var class_object = this;
            var p;
            for(p in obj){
                var in_valid = true;
                var rules = obj[p]['rules'];
                var obj_messages = this.create_error_messages(obj,p);
                for(var i=0;i<rules.length;i++){
                    if(rules[i]=='required'){
                        in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                     
                        if(!obj[p]['valid']){
                         
                         if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                           
                           obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                        }else{
                            var span = document.createElement('span');
                            var br = document.createElement('br');
                            span.style.color = 'red';
                            span.innerHTML=obj_messages[rules[i]];
                            obj[p]['element'].parentElement.appendChild(br);
                            obj[p]['element'].parentElement.appendChild(span);
                        }
                        break;    
                        }else{
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                              
                
                            }        
                        }
                    }
        
                    
                    if(/^min_length_check/.test(rules[i])){
        
        
                        var min_length_value = rules[i].split(':')[0];
                        var min_length_count = rules[i].split(':')[1];
                        in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value,min_length_count);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                        
                        if(!obj[p]['valid']){
                         
                         if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                           
                           obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                        }else{
                            var span = document.createElement('span');
                            var br = document.createElement('br');
                            span.style.color = 'red';
                            span.innerHTML=obj_messages[min_length_value];
                            obj[p]['element'].parentElement.appendChild(br);
                            obj[p]['element'].parentElement.appendChild(span);
                        }
                        break;    
                        }else{
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                              
                
                            }        
                        }
                    }
        
        
        
                     if(/(^equals)/.test(rules[i])){
                        var min_length_value = rules[i].split(':')[0];
                        var min_length_count = rules[i].split(':')[1];
                        in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value,min_length_count);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                      
                        if(!obj[p]['valid']){
                         
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                              
                              obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                           }else{
                               var span = document.createElement('span');
                               var br = document.createElement('br');
                               span.style.color = 'red';
                               span.innerHTML=obj_messages[min_length_value];
                               obj[p]['element'].parentElement.appendChild(br);
                               obj[p]['element'].parentElement.appendChild(span);
                           }
                           break;    
                           }else{
                               if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                   obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                   obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                 
                   
                               }        
                           }
                       }
        
        
                       if(/(^notequals)/.test(rules[i])){
                        var min_length_value = rules[i].split(':')[0];
                        var min_length_count = rules[i].split(':')[1];
                        in_valid = in_valid && class_object.validate_object_test[min_length_value](obj[p]['element'].value,min_length_count);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                      
                        if(!obj[p]['valid']){
                         
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                              
                              obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                           }else{
                               var span = document.createElement('span');
                               var br = document.createElement('br');
                               span.style.color = 'red';
                               span.innerHTML=obj_messages[min_length_value];
                               obj[p]['element'].parentElement.appendChild(br);
                               obj[p]['element'].parentElement.appendChild(span);
                           }
                           break;    
                           }else{
                               if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                   obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                   obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                 
                   
                               }        
                           }
                       }
        
        
                    if(rules[i]=='valid_email'){
                        in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                        
                        if(!obj[p]['valid']){
                         
                         if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                           
                           obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                        }else{
                            var span = document.createElement('span');
                            var br = document.createElement('br');
                            span.style.color = 'red';
                            span.innerHTML=obj_messages[rules[i]];
                            obj[p]['element'].parentElement.appendChild(br);
                            obj[p]['element'].parentElement.appendChild(span);
                        }
                        break;    
                        }else{
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                              
                
                            }        
                        }
                    }
        
        
                    
                    if(/^reg_exp/.test(rules[i])){
        
        
                        var reg_exp_value = rules[i].split(':')[0];
                        var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                        in_valid = in_valid && class_object.validate_object_test[reg_exp_value](obj[p]['element'].value,reg_exp_array);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                        
                        if(!obj[p]['valid']){
                         
                         if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                           
                           obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[reg_exp_value];
                        }else{
                            var span = document.createElement('span');
                            var br = document.createElement('br');
                            span.style.color = 'red';
                            span.innerHTML=obj_messages[reg_exp_value];
                            obj[p]['element'].parentElement.appendChild(br);
                            obj[p]['element'].parentElement.appendChild(span);
                        }
                        break;    
                        }else{
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                              
                
                            }        
                        }
                    }
                    
        
                    if(rules[i]=='valid_number'){
                        in_valid = in_valid && class_object.validate_object_test[rules[i]](obj[p]['element'].value);
                        obj[p]['valid']=in_valid;
                        valid = valid && in_valid;
                        
                        if(!obj[p]['valid']){
                         
                         if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                           
                           obj[p]['element'].parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                        }else{
                            var span = document.createElement('span');
                            var br = document.createElement('br');
                            span.style.color = 'red';
                            span.innerHTML=obj_messages[rules[i]];
                            obj[p]['element'].parentElement.appendChild(br);
                            obj[p]['element'].parentElement.appendChild(span);
                        }
                        break;    
                        }else{
                            if(obj[p]['element'].parentElement.childElementCount>obj[p]['count']){
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                                obj[p]['element'].parentElement.removeChild(obj[p]['element'].parentElement.lastElementChild);
                              
                
                            }        
                        }
                    }
        
                }
            }
            return valid;
        }
        private return_event_type(tagname:any):any{
            return tagname.toLowerCase()=='select'?'change':'keyup';
        }
        private validate(ultimate_form_object:any,form_name:string):any{
            var class_object = this;
            for(var i=0;i<document.forms[form_name].length;i++){
            
                if(ultimate_form_object.hasOwnProperty(document.forms[form_name].elements[i].getAttribute('name'))){
            
                var count = i;
                
                 document.forms[form_name].elements[count].onclick=function(){
                
                var tag = this.tagName;
                var key = this.getAttribute('name');
            
                this.addEventListener(class_object.return_event_type(tag),function(){
            
                    
                    var rules = ultimate_form_object[key]['rules'];
                    var obj_messages = class_object.create_error_messages(ultimate_form_object,key);
                    for(var i=0;i<rules.length;i++){
                        ultimate_form_object[key]['valid']=false;
                        if(rules[i]=='required'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
                             if(class_object.return_event_type(tag)==='change'){
            
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                    this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                 }else{
                 
                                     var span = document.createElement('span');
                                     var br = document.createElement('br');
                                     span.style.color = 'red';
                                     span.innerHTML=obj_messages[rules[i]];
                                     this.parentElement.appendChild(br);
                                     this.parentElement.appendChild(span);
                                 }
            
                             }
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                            continue;
                        }
            
                        
                        else if(/(^min_length_check)/.test(rules[i])){
                            var min_length_value = rules[i].split(':')[0];
                            var min_length_count = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value,min_length_count);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                                if(class_object.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[min_length_value];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
            
                                }
            
            
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        else if(rules[i]=='valid_email'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
            
            
                                if(class_object.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[rules[i]];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
                                
                                }
            
                             
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        
                        else if(/(^reg_exp)/.test(rules[i])){
                            var reg_exp_value = rules[i].split(':')[0];
                            var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                            
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value,reg_exp_array);
                            
                            if(!ultimate_form_object[key]['valid']){
            
                                if(class_object.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[reg_exp_value];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
            
                                }
                             
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
                        
            
                        else if(/(^equals)/.test(rules[i])){
                            var min_length_valuee = rules[i].split(':')[0];
                            var min_length_countt = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                            
                            if(!ultimate_form_object[key]['valid']){
            
                                if(class_object.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[min_length_valuee];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
                                }
                             
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        else if(/(^notequals)/.test(rules[i])){
                            var min_length_valuee = rules[i].split(':')[0];
                            var min_length_countt = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                            
                            if(!ultimate_form_object[key]['valid']){
            
                                if(class_object.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[min_length_valuee];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
                                }
                             
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
                        
            
                        else if(rules[i]=='valid_number'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
                                if(this.return_event_type(tag)==='change'){
            
                                    if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                                        this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                                     }else{
                                         var span = document.createElement('span');
                                         var br = document.createElement('br');
                                         span.style.color = 'red';
                                         span.innerHTML=obj_messages[rules[i]];
                                         this.parentElement.appendChild(br);
                                         this.parentElement.appendChild(span);
                                     }
            
                                }
                             
                             
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
                        
                        
                    }
                    
            
            
                });
                
                this.addEventListener('blur',function(){
                  var rules = ultimate_form_object[key]['rules'];
                
                    var obj_messages = class_object.create_error_messages(ultimate_form_object,key);
                    
                    for(var i=0;i<rules.length;i++){
                        ultimate_form_object[key]['valid']=false;
                        
                        if(rules[i]=='required'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[rules[i]];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                            continue;
                        }
            
                        
                        else if(/(^min_length_check)/.test(rules[i])){
                            var min_length_value = rules[i].split(':')[0];
                            var min_length_count = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_value](this.value,min_length_count);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[min_length_value];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[min_length_value];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        else if(rules[i]=='valid_email'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[rules[i]];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        
                        else if(/(^reg_exp)/.test(rules[i])){
                            var reg_exp_value = rules[i].split(':')[0];
                            var reg_exp_array = rules[i].split(':')[1].split('\\**\\');
                            
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[reg_exp_value](this.value,reg_exp_array);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[reg_exp_value];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
                        
            
                        else if(/(^equals)/.test(rules[i])){
                            var min_length_valuee = rules[i].split(':')[0];
                            var min_length_countt = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[min_length_valuee];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
            
                        else if(/(^notequals)/.test(rules[i])){
                            var min_length_valuee = rules[i].split(':')[0];
                            var min_length_countt = rules[i].split(':')[1];
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[min_length_valuee](this.value,min_length_countt);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[min_length_valuee];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[min_length_valuee];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
                        
            
                        else if(rules[i]=='valid_number'){
                            ultimate_form_object[key]['valid'] = ultimate_form_object[key]['valid'] || class_object.validate_object_test[rules[i]](this.value);
                            
                            if(!ultimate_form_object[key]['valid']){
                             
                             if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                               
                               this.parentElement.lastChild.innerHTML = obj_messages[rules[i]];
                            }else{
                                var span = document.createElement('span');
                                var br = document.createElement('br');
                                span.style.color = 'red';
                                span.innerHTML=obj_messages[rules[i]];
                                this.parentElement.appendChild(br);
                                this.parentElement.appendChild(span);
                            }
                            break;    
                            }else{
                                if(this.parentElement.childElementCount>ultimate_form_object[key]['count']){
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                    this.parentElement.removeChild(this.parentElement.lastElementChild);
                                  
                    
                                }        
                            }
                        }
            
                    }
            
            
                });
            
            
                            
                }
            
                }
                
                
            }

        }       
}