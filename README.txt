# Javascript Form Validation Script

A simple HTML form validation script written in Typesript. Works with HTML's data attributes and that's where all the initializations are done. Uses two data attributes, "data-messages" attribute and "data-rules" attribute. Initialize your rules and error messages. Validates for minimum number of characters, emails, regular expressions, matching fields and non-matching fields, required fields and many more to come. It's pure Typescript/Javascript and does not require the use of an external library like JQuery. Just download or clone the repo, import the file in your new Typescript file, like for example, 
import { Validate_Form } from './Validate_Form';
let form = new Validate_Form('form_name_as_defined_in_your_html_file');
The Test_Form.ts file just imports the Validation_Form class from Validation_Form.ts and its compiled into it's javascript's equivalent to produce Test_Form.js. Test_Form_Output.js is the browserified version of Test_Form.js and this is the file you will need in your web folder.

Compile to javascript, and use a library like browserify to re-run compilation for utmost compatibility with browsers when the browser comes across statements like imports and exports and you're good to go.

Initialize html data attributes like below:
<input type="text" name="username" data-rules="required|min_length_check:5" data-messages-'required-Please enter your username|min_length_check-Ensure your username has at least 5 characters'>
<input type="text" name="email" data-rules='required|valid_email' data-messages='required-Please fill in your email address|valid_email-Enter a valid email address'>
<input type="text" name="regular_expression_check" data-rules='required|reg_exp:^leonard\\**\\^joey' data-messages='required-this field is required|reg_exp-text must start with "leonard" or "joey"'>

Note: the rules and messages are separated by the pipe character, messages are indicated in this manner rule-rule's message, and rules that require parameters like min_length_check and reg_exp, the parameter(s) is/are placed on the RHS of the rule name separated by a colon eg, "reg_exp:regular_expression1\\**\\regular_expression2". Note that when validating against more than one regular expression, separate with this characters "\\**\\" without the quotation marks. The script uses this as a separator "\\**\\" when running through regular expression rules. Messages are in the form rule dash rule's message eg. valid_email-Please provide a valid email address, with no spaces in between. 

Hidden field and fields without the data-rules and data-messages attributes are ignored eg
<input type="hidden" name="something_the_user_should_not_see" value='secret'>
<input type="text" name="unimportant">

RULES
*required
*valid_email
*reg_exp
*min_length_check
*equals
*notequals
*valid_number

More rules are gonna be added from time to time